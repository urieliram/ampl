#######   #######  #######  #######  #######    #######
#######   Multiple trip Center Customer VRP     #######
#######   Programador: Uriel Iram Lezama Lope   #######
#######   Posgrado en Ing Sistemas, PISIS, UAnL	#######
#######   #######  #######  #######  #######    #######

#######  CONJUNTOS
#  nombre de Conjunto  # {indice usado en la ecuacion} Descripcion del conjunto.
set N;			            	                # ciudades + copias de depositos  N = n + m - 1
set n;			            	                # clientes

#######  PARAMETROS 
param c { N , N } ;  # costos de ciudad i a la ciudad j
param d { N } ;		 # demanda del cliente
param D ;            # jornada de trabajo
param Q ;            # capacidad vehiculo

#######  VARIABLES
var x { N , N } binary;		# 
var y { N , N , N } >=0;	# (1)  Si se usa el arco, (0) si no se usa
var w { N } 		>=0;	# demanda acumulada

#######  FUNCION OBJETIVO
minimize Z :
sum{ i in N:i<>0 } c[0,i] * x[i,1] * (card{N}-1) + 
sum{ i in N:i<>0 } sum{ j in N:(j<>0 and i<>j) } ( c[ i , j ] * 
sum{ k in N:(k<>0 and k<>(card(N)-1)) } (card(N)-k-1) * y[i,j,k] ) ;

###################  restriccion 14
subject to res14{ i in N: i<>0 }: 
sum { k in N:k<>0 } x[ i , k ] = 1 ;

###################  restriccion 15
subject to res15{ k in N: k<>0 }: 
sum { i in N:i<>0 } x[ i , k ] = 1 ;

###################  restriccion 16
subject to res16{ i in 1..(card(N)-1), k in 1..(card(N)-1-1)}: 
sum { j in N:j<>0 and j<>i} y[ i , j , k] = x[ i , k ] ;

###################  restriccion 17
subject to res17{ i in 1..(card(N)-1), k in 1..(card(N)-1-1)}: 
sum { j in N:j<>0 and j<>i} y[ j , i , k ] = x[ i , k + 1] ;

###################  restriccion 18
subject to res18: 
sum{ i in N:i<>0 } c[0,i] * x[i,1] + 
sum{ i in N:i<>0 } sum{ j in N:(j<>0 and i<>j) } ( c[ i , j ] * 
sum{ k in N:(k<>0 and k<>(card(N)-1)) } y[i,j,k] ) +
sum{ i in N:i<>0 } c[i,0] * x[i,card(N)-1] <= D;

###################  restriccion 19
subject to res19: 
sum { i in n } d[ i ] * x[ i , 1 ] = w[ 1 ];

###################  restriccion 20
subject to res20{ k in 1..(card(N)-1-1) }:
w[ k + 1 ] >= w[ k ] +
sum{ i in n } d[ i ] * x[ i , k + 1 ] +
Q * (sum{ i in n }  x[ i , k + 1 ] - 1);

###################  restriccion 21
subject to res21{ k in 1..(card(N)-1-1) }: 
 w[ k + 1 ] <= Q * sum { i in n } x[ i , k + 1 ]; 
 






M







































